module.exports = function (arr, callback) {

    var withCallback = (typeof callback === 'function');

    try
    {
        var res = '';
        var rangeStart;
        var rangeEnd;

        for(var i = 0; i < arr.length; i++)
        {
            if(i == 0)
            {
                res = res + arr[i];
                rangeStart = arr[i];
                rangeEnd = arr[i];
            }
            else
            {
                if(arr[i] - rangeEnd == 1)
                {
                    rangeEnd = arr[i];
                    if(i == arr.length - 1)
                    {
                        if(rangeEnd - rangeStart == 1)
                        {
                            res = res + ',' + arr[i];
                        }
                        else
                        {
                            res = res + '-' + arr[i];
                        }
                    }
                }
                else
                {
                    if(rangeStart != rangeEnd)
                    {
                        if(rangeEnd - 1 == rangeStart)
                        {
                            res = res + ',' + rangeEnd + ',' + arr[i];
                        }
                        else
                        {
                            res = res + '-' + rangeEnd + ',' + arr[i];
                        }
                    }
                    else
                    {
                        res = res + ',' + arr[i];
                    }

                    rangeStart = arr[i];
                    rangeEnd = arr[i];
                }
            }
        }

        withCallback && callback(null, res);
    }
    catch (e)
    {
        if(withCallback)
        {
            callback(e);
        }
        else
        {
            throw(e);
        }
    }
}
