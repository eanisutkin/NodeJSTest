var arrToRanges = require('../arrToRanges.js');
var expect = require('chai').expect;

describe('#arrToRanges()', function() {
    context('with array argument', function() {

        it('should return a string with ranges equal to "1-8"', function(done) {
            arrToRanges([1,2,3,4,5,6,7,8], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1-8');

                done();
            });
        });

        it('should return a string with ranges equal to "1,3-8"', function(done) {
            arrToRanges([1,3,4,5,6,7,8], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1,3-8');

                done();
            });
        });

        it('should return a string with ranges equal to "1,3-8,10-12"', function(done) {
            arrToRanges([1,3,4,5,6,7,8,10,11,12], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1,3-8,10-12');

                done();
            });
        });

        it('should return a string with ranges equal to "1-3"', function(done) {
            arrToRanges([1,2,3], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1-3');

                done();
            });
        });

        it('should return a string with ranges equal to "1,2"', function(done) {
            arrToRanges([1,2], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1,2');

                done();
            });
        });

        it('should return a string with ranges equal to "1,2,4"', function(done) {
            arrToRanges([1,2,4], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1,2,4');

                done();
            });
        });

        it('should return a string with ranges equal to "1,2,4-6"', function(done) {
            arrToRanges([1,2,4,5,6], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1,2,4-6');

                done();
            });
        });

        it('should return a string with ranges equal to "1-3,7-9,15,17,19-21"', function(done) {
            arrToRanges([1,2,3,7,8,9,15,17,19,20,21], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1-3,7-9,15,17,19-21');

                done();
            });
        });

        it('should return a string with ranges equal to "1-6,100,1091,1999-2002"', function(done) {
            arrToRanges([1,2,3,4,5,6,100,1091,1999,2000,2001,2002], function(err, res) {
                if(err)
                {
                    return done(err);
                }

                expect(res)
                    .to.be.a('string')
                    .and.equal('1-6,100,1091,1999-2002');

                done();
            });
        });

        it('should return a string with ranges equal to \'[1-3,5,7-9]\'', function(done) {
            arrToRanges([1,2,3,5,7,8,9], function(err, res) {
                if(err) return done(err);

                expect(res)
                    .to.be.a('string')
                    .and.equal('1-3,5,7-9');

                done();
            });
        });

        it('should return a string with ranges equal to "1"', function(done) {
            arrToRanges([1], function(err, res) {
                if(err) return done(err);

                expect(res)
                    .to.be.a('string')
                    .and.equal('1');

                done();
            });
        });

        it('should return a string with ranges equal to "1,3,5,7,9,11"', function(done) {
            arrToRanges([1,3,5,7,9,11], function(err, res) {
                if(err) return done(err);

                expect(res)
                    .to.be.a('string')
                    .and.equal('1,3,5,7,9,11');

                done();
            });
        });

    });
});
